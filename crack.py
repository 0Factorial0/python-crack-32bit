def clear():
    import os
    #clear screen
    test_os = os.name
    if test_os == "nt":
        os.system('cls')
    elif test_os == "posix":
        os.system('clear')
    else:
        pass

def crack():
    import os
    import random
    import time

    #clear screen
    clear()

    try:
        #info
        print("--------------------------")
        print("Generating Random Password...")
        print("--------------------------")

        #generate password
        randomPass = random.randint(1,4294967296)
        startingTime = time.time()
        print("Cracking...")
        print("--------------------------")
        print("Starting time: {0}".format(startingTime))
        print("--------------------------")

        #try all 32 bit integers
        for i in range(1,4294967296):
            #if found stop and print
            if i == randomPass:
                finishingTime = time.time()
                timeToSolve = finishingTime - startingTime
                timeToSolve = timeToSolve/1000
                print("--------------------------")
                print("Finishing time: {0}".format(finishingTime))
                print("--------------------------")
                print("Got It.")
                print("Password was {0}".format(randomPass))
                print("Time To Solve: {0} seconds".format(timeToSolve))
                print("--------------------------")
    except:
        print("--------------------------")
        print("Unexpected Error.")
        time.sleep(2)
        crack()

crack()